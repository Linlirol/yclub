cmake_minimum_required(VERSION 3.15)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
project(yclub)


file (GLOB_RECURSE LIB_SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")
add_library(club_misc STATIC ${LIB_SOURCES})

target_include_directories(club_misc PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include/")

add_executable(${PROJECT_NAME} yclub.cpp)
target_link_libraries(${PROJECT_NAME} club_misc)

add_subdirectory(thirdparty)
if (TESTS)
    enable_testing()
    add_subdirectory(tests)
    message("TESTING ENABLED")
endif()
