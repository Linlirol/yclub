#pragma once

#include <string>

namespace types {

/* Event ID */
enum class ID {
    inCome   = 1,
    inSit    = 2,
    inWait   = 3,
    inGo     = 4,

    outGo    = 11,
    outSit   = 12,
    outError = 13,
};

/* Type for convenient storage of event data */
struct event_t final {
    int time;
    ID  e_id;
    std::string name; // customer name or error body
    int table = 0;

    std::string str() const;
};

/* Type for convenient storage of table data */
struct table_t final {
    bool is_busy = false;
    int sum_time = 0;
    int sum_rev  = 0;
    int busy_since = 0;

    void free(int cost, int curr_time);

    void sit(int curr_time);
};

} // namespace types;
