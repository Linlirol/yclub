#pragma once
#include <string>
#include <utility>

#include <Types.h>

namespace parser {

/* Converts "HH:MM"-formated time string to minutes int */
int stotime(std::string const &str);

/* Convertes time in minutes to "HH:MM"-formated time string */
std::string timetos(int const time);

/* Parses the unsigned int string */
int parse_uint(std::string const &str);

/* Converts "HH:MM HH:MM" string to pair of minute ints */
std::pair<int, int> parse_txt(std::string const &str);

/* Converts "HH:MM ID <body>" string to event_t */
types::event_t parse_event(std::string const &str);

} // namespace parser;
