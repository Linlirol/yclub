#include <gtest/gtest.h>

#include <string>
#include <utility>
#include <exception>

#include "Parser.h"
#include "Types.h"
using namespace parser;
using namespace  types;

TEST(parser, stotime) {
    ASSERT_EQ(stotime("20:23"), 1223);
    ASSERT_EQ(stotime("00:00"),    0);
    ASSERT_EQ(stotime("23:59"), 1439);
    ASSERT_EQ(stotime("12:11"),  731);
    ASSERT_EQ(stotime("10:41"),  641);
}

TEST(parser, timetos) {
    EXPECT_EQ(timetos(1234), "20:34"); // EXPECT_STREQ's for C-str
    EXPECT_EQ(timetos(1111), "18:31");
    EXPECT_EQ(timetos(0)   , "00:00");
    EXPECT_EQ(timetos(1439), "23:59");
}

TEST(parser, time_mixed) {
    ASSERT_EQ(stotime(timetos(1223)), 1223);
    ASSERT_EQ(stotime(timetos(0))   ,    0);
    ASSERT_EQ(stotime(timetos(1200)), 1200);
}

TEST(parser, parse_txt) {
    // correct data
    std::string c1 = "10:20 23:59";
    std::string c2 = "00:00 00:00";
    std::string c3 = "01:02 03:04";

    // incorrect data
    std::string ic1 =   "1:02 00:00";
    std::string ic2 = "01:02  11:21";
    std::string ic3 = "20:30 20:30 ";
    std::string ic4 =  "24:00 10:00";
    std::string ic5 = "-10:00 20:30";
    std::string ic6 =  "10:20 23:60";
    std::string ic7 =  "10:20 31:59";

    //
    ASSERT_EQ(std::make_pair(620, 1439), parse_txt(c1));
    ASSERT_EQ(std::make_pair(0, 0),      parse_txt(c2));
    ASSERT_EQ(std::make_pair(62, 184),   parse_txt(c3));

    EXPECT_THROW(parse_txt(ic1), std::runtime_error);
    EXPECT_THROW(parse_txt(ic2), std::runtime_error);
    EXPECT_THROW(parse_txt(ic3), std::runtime_error);
    EXPECT_THROW(parse_txt(ic4), std::runtime_error);
    EXPECT_THROW(parse_txt(ic5), std::runtime_error);
    EXPECT_THROW(parse_txt(ic6), std::runtime_error);
    EXPECT_THROW(parse_txt(ic7), std::runtime_error);
}

TEST(parser, parse_uint) {
    // correct data
    std::string c1 =     "17";
    std::string c2 = "923847";
    std::string c3 =   "4785";
    std::string c4 =      "2";
    std::string c5 =     "10";

    // incorrect data
    std::string ic1 =   "01";
    std::string ic2 =    "0";
    std::string ic3 = "0123";
    std::string ic4 =   "-4";

    //
    ASSERT_EQ(parse_uint(c1),     17);
    ASSERT_EQ(parse_uint(c2), 923847);
    ASSERT_EQ(parse_uint(c3),   4785);
    ASSERT_EQ(parse_uint(c4),      2);
    ASSERT_EQ(parse_uint(c5),     10);

    ASSERT_THROW(parse_uint(ic1), std::runtime_error);
    ASSERT_THROW(parse_uint(ic2), std::runtime_error);
    ASSERT_THROW(parse_uint(ic3), std::runtime_error);
    ASSERT_THROW(parse_uint(ic4), std::runtime_error);
}

TEST(parser, parse_event) {
    // correct data
    std::string c1 = "01:02 1 na_me-1";
    std::string c2 = "00:00 2 md123 3";
    std::string c3 = "09:14 2 bruh 14";
    std::string c4 = "12:34 3 sigsegv";
    std::string c5 = "23:59 4 _as-d1_";

    // incorrect data
    std::string ic1 = "24:00 1 name123";
    std::string ic2 = " 2:32 2 namesss";
    std::string ic3 = "01:02 2 nonumss";
    std::string ic4 = "13:00 2 yclub 0";
    std::string ic5 = "12:00 3 nums 23";
    std::string ic6 = "12:20 3  missed";

    //
    EXPECT_EQ(parse_event(c1).str(), c1);
    EXPECT_EQ(parse_event(c2).str(), c2);
    EXPECT_EQ(parse_event(c3).str(), c3);
    EXPECT_EQ(parse_event(c4).str(), c4);
    EXPECT_EQ(parse_event(c5).str(), c5);

    ASSERT_THROW(parse_event(ic1), std::runtime_error);
    ASSERT_THROW(parse_event(ic2), std::runtime_error);
    ASSERT_THROW(parse_event(ic3), std::runtime_error);
    ASSERT_THROW(parse_event(ic4), std::runtime_error);
    ASSERT_THROW(parse_event(ic5), std::runtime_error);
    ASSERT_THROW(parse_event(ic6), std::runtime_error);
}

TEST(types, event) {
    // event_t = {time, ID, name, table};
    event_t c1{  0, ID::inCome, "name"};
    event_t c2{ 62, ID::inSit,  "name", 123};
    event_t c3{720, ID::inWait, "eman"};
    event_t c4{803, ID::inGo,   "mena"};

    event_t c5{1439, ID::outGo,    "enma"};
    event_t c6{ 650, ID::outSit,   "amen", 3};
    event_t c7{ 660, ID::outError, "anem"};

    EXPECT_EQ(c1.str(), "00:00 1 name");
    EXPECT_EQ(c2.str(), "01:02 2 name 123");
    EXPECT_EQ(c3.str(), "12:00 3 eman");
    EXPECT_EQ(c4.str(), "13:23 4 mena");

    EXPECT_EQ(c5.str(), "23:59 11 enma");
    EXPECT_EQ(c6.str(), "10:50 12 amen 3");
    EXPECT_EQ(c7.str(), "11:00 13 anem");
}

TEST(types, table) {
    // table_t = {is_busy, sum_time, sum_revenue, busy_since};

    table_t t;

    int h_cost = 10;

    t.sit(0);
    ASSERT_TRUE(t.is_busy);

    t.free(h_cost, 1);
    ASSERT_FALSE(t.is_busy);

    ASSERT_EQ(t.sum_rev, h_cost);
    ASSERT_EQ(t.sum_time,     1);

    t.sit(1);
    ASSERT_TRUE(t.is_busy);

    t.free(h_cost, 60);
    ASSERT_FALSE(t.is_busy);

    ASSERT_EQ(t.sum_rev,  2 * h_cost);
    ASSERT_EQ(t.sum_time,         60);

    t.sit(60);
    ASSERT_TRUE(t.is_busy);

    t.free(h_cost * 2, 1260);
    ASSERT_FALSE(t.is_busy);

    ASSERT_EQ(t.sum_rev, (2 + 40) * h_cost);
    ASSERT_EQ(t.sum_time,             1260);
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}

