#include <string>
#include <sstream>
#include <format>
#include <exception>
#include <utility>
#include <regex>

#include "Parser.h"
#include "Types.h"

namespace parser {

int stotime(std::string const &str) {
    int time = 0;
    time += 60 * std::stoi(str.substr(0, 2));
    time +=      std::stoi(str.substr(3, 2));
    return time;
}

std::string timetos(int const time) {
    return std::format("{:02d}:{:02d}", time / 60, time % 60);
}

int parse_uint(std::string const &str) {
    /* regex for positive integer string */
    static std::regex const uint_pattern("[1-9]+\\d*");

    if (!std::regex_match(str, uint_pattern))
        throw(std::runtime_error(str));

    return std::stoi(str);
}

std::pair<int, int> parse_txt(std::string const &str) {
    /* regex for "HH:MM HH:MM" string */
    static std::regex const txt_pattern("([01]\\d|2[0-3]):([0-5]\\d) ([01]\\d|2[0-3]):([0-5]\\d)");

    if (!std::regex_match(str, txt_pattern))
        throw(std::runtime_error(str));

    std::string time;
    std::stringstream ss(str);

    std::pair<int, int> timextime;

    ss >> time;
    timextime.first  = stotime(time);

    ss >> time;
    timextime.second = stotime(time);

    return timextime;
}

types::event_t parse_event(std::string const &str) {
    /* regex for "HH:MM ID <name>" string */
    static std::regex const event_pattern(
            "((([01]\\d|2[0-3]):([0-5]\\d)) [134] [a-z\\d_-]+)|(([01]\\d|2[0-3]):([0-5]\\d) 2 [a-z\\d_-]+ [1-9]+\\d*)"
        );

    if (!std::regex_match(str, event_pattern))
        throw(std::runtime_error(str));

    types::event_t event;

    std::string curr;
    std::stringstream ss(str);

    // time
    ss >> curr;
    event.time = stotime(curr);

    // id
    int id;
    ss >> id;

    event.e_id = static_cast<types::ID>(id);

    // name
    ss >> curr;
    event.name = curr;

    // # of table
    if (event.e_id == types::ID::inSit) {
        ss >> id;
        event.table = id;
    }

    return event;
}

} // namespace parser;
