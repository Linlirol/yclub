#include <string>
#include <format>
#include <iterator>

#include "Types.h"
#include "Parser.h"

namespace types {

std::string event_t::str() const {
    std::string result = std::format("{} {} {}", parser::timetos(time), static_cast<int>(e_id), name);

    if (e_id == ID::inSit || e_id == ID::outSit)
        std::format_to(std::back_inserter(result), " {}", table);

    return result;
}

void table_t::free(int cost, int curr_time) {
    is_busy = false;
    int minutes = curr_time - busy_since;
    sum_time += minutes;

    int hours = minutes % 60 ? minutes / 60 + 1 : minutes / 60;
    sum_rev += hours * cost;
}

void table_t::sit(int curr_time) {
    is_busy = true;
    busy_since = curr_time;
}

} // namespace types;
