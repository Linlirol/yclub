#include <exception>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>
#include <map>
#include <list>

#include <format>
#include <sstream>
#include <fstream>
#include <iostream>

#include "Parser.h"
#include "Types.h"

std::ostream& operator<<(std::ostream& os, const types::event_t& ev) {
    os << ev.str();
    return os;
}

int main(int argc, char* argv[]) {
    try {
        /* Open file */
        if (argc != 2) {
            std::cerr << "Incorrect usage. Try again: yclub.exe [file]\n";
            return 1;
        }
        auto const file = argv[1];

        std::ifstream clubInput(file);
        if (!clubInput.is_open()) {
            std::cerr << "Error! No such file or directory: " << file << std::endl;
            return 1;
        }

        /* --- */

        /* Input parser */
        std::string str;

        // table num
        std::getline(clubInput, str);

        int table_num = parser::parse_uint(str);

        // working hours
        std::getline(clubInput, str);

        auto [opening, closing] = parser::parse_txt(str);

        // cost of hour
        std::getline(clubInput, str);

        int h_cost = parser::parse_uint(str);

        // events
        std::vector<types::event_t> events;

        while(std::getline(clubInput, str)) {
            types::event_t event = parser::parse_event(str);
            if ((!events.empty() && events.back().time > event.time) || (event.table > table_num))
                throw(std::runtime_error(str));
            
            events.push_back(event);
        }
        /* --- */

        std::cout << parser::timetos(opening) << std::endl; // opening time

        /* Events handler */

        std::map<std::string, int>  clients; // name : table
        std::vector<types::table_t> tables(table_num + 1); // tables[i] = table_i
        std::list<std::string>      queue;   // table queue; std::list is used instead of std::queue for erase support (ID 4)

        auto const shift_queue = [&](int const t_ind, int const curr_time) {
            if (!queue.empty() && curr_time < closing) {
                std::string name = queue.front();
                queue.pop_front();

                tables[t_ind].sit(curr_time);
                clients[name] = t_ind;

                std::cout << types::event_t{curr_time, types::ID::outSit, name, t_ind} << std::endl;
            }
        };


        for (types::event_t& ev : events) {
            std::cout << ev << std::endl;

            int t_ind = 0;

            switch(ev.e_id) {
                case types::ID::inCome:
                    if (ev.time < opening || ev.time >= closing)
                        std::cout << types::event_t{ev.time, types::ID::outError, "NotOpenYet"} << std::endl;

                    else if (clients.contains(ev.name))
                        std::cout << types::event_t{ev.time, types::ID::outError, "YouShallNotPass"} << std::endl;

                    else
                        clients[ev.name] = 0;

                    break;

                case types::ID::inSit: // presuming event.table <= table_num
                    if (!clients.contains(ev.name)) {
                        std::cout << types::event_t{ev.time, types::ID::outError, "ClientUnknown"} << std::endl;
                        break;
                    }


                    if (tables[ev.table].is_busy) {
                        std::cout << types::event_t{ev.time, types::ID::outError, "PlaceIsBusy"} << std::endl;
                        break;
                    }

                    t_ind = clients[ev.name];

                    if (t_ind) // client is already at the table
                        tables[t_ind].free(h_cost, ev.time); // set occupied table free

                    clients[ev.name] = ev.table;
                    tables[ev.table].sit(ev.time); // sit at the new table

                    break;

                case types::ID::inWait:
                    if (!clients.contains(ev.name)) {
                        std::cout << types::event_t{ev.time, types::ID::outError, "ClientUnknown"} << std::endl;
                        break;
                    }

                    if (std::find_if(tables.begin() + 1, tables.end(), [](auto const &t){return (!t.is_busy);}) != tables.end())
                        std::cout << types::event_t{ev.time, types::ID::outError, "ICanWaitNoLonger!"} << std::endl;

                    else if (queue.size() > table_num) { // queue overflow
                        std::cout << types::event_t{ev.time, types::ID::outGo, ev.name} << std::endl;
                        clients.erase(ev.name);
                    }

                    else
                        queue.push_back(ev.name);

                    break;

                case types::ID::inGo:
                    if (!clients.contains(ev.name)) {
                        std::cout << types::event_t{ev.time, types::ID::outError, "ClientUnknown"} << std::endl;
                        break;
                    }

                    t_ind = clients[ev.name];
                    if (t_ind) {
                        tables[t_ind].free(h_cost, ev.time);
                        shift_queue(t_ind, ev.time);
                    }

                    else {
                        auto it = std::find(queue.begin(), queue.end(), ev.name);
                        if (it != queue.end())
                            queue.erase(it);
                    }

                    clients.erase(ev.name);

                    break;

                default:
                    break;
            }
        } // ev : events

        /* --- */

        /* Closing time! */

        for (auto&& [name, t_ind] : clients) { // std::map provides alphabetical order
            std::cout << types::event_t{closing, types::ID::outGo, name} << std::endl;
            if (t_ind)
                tables[t_ind].free(h_cost, closing);
        }

        std::cout << parser::timetos(closing) << std::endl;

        for (int i = 1; i <= table_num; ++i) {
            std::cout << std::format("{} {} {}", i, tables[i].sum_rev, parser::timetos(tables[i].sum_time)) << std::endl;
        }

        /* --- */

    }
    catch(std::runtime_error &e) { std::cout << e.what() << std::endl; }
    catch(std::exception &e)     { std::cerr << e.what() << std::endl; }
    catch(...) {
        std::cout << "Unrecognized exception caught\n";
    }
}
